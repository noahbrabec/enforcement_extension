CURRENTLY WORKS ON THESE WEBSITES:

https://my.hostafrica.co.za/submitticket.php?step=2&deptid=6

https://www.billing.blueangelhost.com/submitticket.php?step=2&deptid=10

https://www.interserver.net/contact-information.html

https://www.liquidweb.com/contact-us/abuse/

https://www.ovh.com/world/abuse/

https://abuse.web.com/

WHATS BEEN DONE:
the current functionality of my code:
	- scrapes the info under attributes on summary page (domain, ip, url, etc.)
	- scrapes the subject and body of the email
	- scrapes the most recent image (if any) to download and attach to forms via a link on the popup
	- uses your registrar email match method to open appropriate form from enforcement email page
	- fills some of the basic forms that only have one document.forms element and which have meaningful id names i.e. the id's contain keywords such as url, ip, domain, etc.
	- triggers page alerts if certain conditions go amiss/are violated such as
		- trying to fill form without riskiq event enforcement page open
		- trying to fill form with no template selected
		- trying to open form from enforcement page without registrar email present/with faulty email present
		- image link clicked, but there are no images to be downloaded

usage:
my extension has three buttons: 
	- 'REPORT EVENT' is to be used when on the riskiq event enforcement email page. it uses your switch case function to determine which form to open, and opens it. if there is an issue with the registrar email, one of the alerts mentioned above (line 8) will be triggered
	- 'Fill Form' is to be used when on a form page (shocking i know). currently it is only able to fill forms which meet the specific conditions mentioned above (line 7)
	- 'Download and attach this image' is to be clicked when on a form page which allows you to attach a file. currently it only provides the most recent crawl snapshot, but many of the forms allow you to input multiple files so i think it would be a good idea to modify it to open all available snapshots (it would be even better if you figured out how to directly download them from that button, i didn't have enough time to see if that was possible).

some notes:
	- the names of each json key are the same as the attributes page they correspond to. the others are Event_Type (phishing, domain infringement, etc.), Message_Subject and Message_Body (subject and body of enforcement email), Image_Source, and Registrar_Email (the last two will have a value of '' if none is present)
	- multi-word key names in the json object containing the scraped data which is parsed by the content script are modified to have all spaces replaced by an underscore. ex: "Initial URLs" -> "Initial_URLs"
	- domain infringement events have an "Initial_URL" attribute and phishing events have "Initial_URLs", just a tiny detail that could be annoying if unnoticed


hopefully i've been semi-successful in describing what my code does, and how. i also hope it's useful to you! lol
if you have any questions about what i've done here or what i intended, email me at bethanylong@ucsb.edu (unless my slack acct doesn't get deactivated? in which case slack is still a good way to reach me)
i didn't really get to work with anyone else during my internship here, so despite how short it lasted, it was really nice working with you! :) good luck with your school year and everything!