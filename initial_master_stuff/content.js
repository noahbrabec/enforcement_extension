
//test function to make sure message passing is working and that the script is running
chrome.runtime.sendMessage({dom: document}, function (response) {
    console.log(response.received);
});

//Allows users to click to check if there is a registrars email that is associate to a web form in a template
document.getElementById("webformButton").addEventListener('click', () => {
    modifyDOM();
});

/*
chrome.pageAction.onClicked.addListener(function(tab){
    modifyDOM();
});

 */

//switch containing *most* of the web form translations
function checkEmail(email) {
    switch(email){
        case "abuse@godaddy.com":
            window.open("https://supportcenter.godaddy.com/AbuseRepolsrt/");
            break;
        case "abuse@dynadot.com":
            window.open("https://google.com/");
            break;
        case "abuse@incapsula.com":
            window.open("https://my.imperva.com");
            break;
        case "abusencc@interserver.net":
            window.open("https://www.interserver.net/contact-information.html");
            break;
        case "onlinenic-partner@onlinenic.com":
            window.open("https://support.onlinenic.com/");
            break;
        case "abuse@liquidweb.com":
            window.open("https://www.liquidweb.com/contact-us/");
            break;
        case "abuse-iplan@IPLAN.COM.AR":
            window.open("https://www.iplan.com.ar/Iplan/Contacto");
            break;
        case "abuse@digitalocean.com":
            window.open("https://www.digitalocean.com/company/contact/#abuse");
            break;
        case "abuse@limestonenetworks.com":
            window.open("https://www.limestonenetworks.com/about/report-abuse.html");
            break;
        case "abuse@ovh.net":
            window.open("https://www.ovh.com/world/abuse/");
            break;
        case "intl-abuse@list.alibaba-inc.com":
            window.open("https://www.alibabacloud.com/report#abuse");
            break;
        case "google-cloud-compliance@google.com":
            window.open("https://support.google.com/legal/troubleshooter/1114905#ts=1115658%2C1115684");
            break;
        case "abuse@abusehost.ru":
            window.open("http://abusehost.ru/");
            break;
        case "domain-admin@yola.com":
            window.open("https://www.yola.com/support/contact/report-abuse#phishing");
            break;
        case "abuse@microsoft.com":
            window.open("https://portal.msrc.microsoft.com/en-us/engage/cars");
            break;
        case "andys@co.ru":
            window.open("https://app.convercent.com/en-us/LandingPage/21dbfbe1-7697-e511-8123-00155d623368");
            break;
        case "abuse-contact@publicdomainregistry.com":
            window.open("https://publicdomainregistry.com/report-abuse-complain/");
            break;
        case "abuse@turnkeyinternet.net":
            window.open("https://helpdesk.turnkeyinternet.net/Tickets/Submit");
            break;
        case "abuse@namesilo.com":
            window.open("https://new.namesilo.com/phishing_report.php");
            break;
        case "abuse@quadranet.com":
            window.open("https://www.quadranet.com/contact");
            break;
        case "abuse@namecheap.com":
            window.open("https://support.namecheap.com/index.php?/Tickets/Submit&_ga=2.188721002.372030028.1553642554-811107515.1553642554");
            break;
        case "abuse@amazonaws.com":
            window.open("https://aws.amazon.com/forms/report-abuse");
            break;
        case "abuse@staff.aruba.it":
            window.open("https://www.aruba.it/en/report-abuse.aspx");
            break;
        case "abuse@szervernet.hu":
            window.open("https://www.szervernet.hu/kapcsolat");
            break;
        case "alexx.person@gmail.com":
            window.open("https://internetprivatehosting.com/#");
            break;
        case "abuse@web.com":
            window.open("https://abuse.web.com/");
            break;
        default:
            break;
    }
}

function modifyDOM() {
    console.log('Tab script:');
    console.log(document.body);

    let test = document.querySelectorAll("[data-qa='multiBoxItem']");
    console.log("Printing Test: \n");

    console.log("Which is the registrar");
    for(let i = 0; i < test.length; i++){
        let temp = test.item(i);
        let title = temp.querySelector("[data-qa='multiBoxItemTitle']");
        if(title.innerHTML.includes("REGISTRAR")) {
            let email = temp.querySelector("[data-qa='multiBoxItemText']").innerHTML;
            checkEmail(email);
            console.log("Registrar Email: " + email);
        }
    }
    let web_form_url = "https://google.com/";
    return test;
}
