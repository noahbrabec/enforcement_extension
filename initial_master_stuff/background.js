
//catches messages, calls appropriate functions
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
        console.log(sender.tab ?
                    "From a content script:" + sender.tab.url:
                    "From the extension");
        if (request.dom !== null) {
            console.log(request.dom);
            sendResponse({received: true});
            chrome.pageAction.show(sender.tab.id);
        }
        else
            sendResponse({received: false});
    });