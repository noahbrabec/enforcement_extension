
go();

function go() {
	let dataPort = chrome.runtime.connect({name: "dataPort"});
	dataPort.onMessage.addListener(function(msg) {
		switch (msg.type) {
			case 'image':
                getImage(dataPort);
                break;
            case 'fill':
            	getMoreData(dataPort);
                break;
            case 'openForm':
            	getForm(dataPort);
                break;
            default:
                console.log('DEFAULT - SOMETHING IS WRONG');
                break;
		}
	});
};

//////////////////    RETURNS MOST RECENT CRAWL SNAPSHOT OR '' IF NOT PRESENT   ///////////////////////////

function getImage(dataPort) {
	src = '';
	if (document.getElementsByClassName('detail-content-snapshots-snapshot-image-thumb').length > 0) {
		src = document.getElementsByClassName('detail-content-snapshots-snapshot-image-thumb')[1].src;
		if (src == '') {
			src = document.getElementsByClassName('detail-content-snapshots-snapshot-image-thumb')[0].src;
		}
	}
	
	json_text = '{"Image_Source" : "' + src + '"}';


	console.log(json_text);
	dataPort.postMessage({data: json_text, log: json_text});
};

//////////////////    RETURNS THE REGISTRAR'S EMAIL OR '' IF NOT PRESENT  ///////////////////////////

function getForm(dataPort) {
	let email = '';

	let boxes = document.querySelectorAll("[data-qa='multiBoxItem']");

    for(let i = 0; i < boxes.length; i++){
        let title = boxes[i].querySelector("[data-qa='multiBoxItemTitle']");
        if(title.innerText == "REGISTRAR") {
            email = boxes[i].querySelector("[data-qa='multiBoxItemText']").innerHTML;
            console.log("Registrar Email: " + email);
            break;
        }
    }

    json_text = '{"Registrar_Email" : "' + email + '"}'

	console.log(json_text);
	dataPort.postMessage({data: json_text, log: json_text});
};


//////////////////    RETURNS ATTRIBUTES ON SUMMARY PAGE, EVENT TYPE, MESSAGE SUBJECT AND BODY FROM EMAIL  ///////////////////////////

function getMoreData(dataPort) {
    //This grabs the list of 'sections' from the site detail page
    let siteDetails = document.getElementsByClassName("detail-container event-site-details")[0].getElementsByClassName("detail-column-12")[0].children;

    let asset = siteDetails[0].children;
    let whoisInformation = siteDetails[5].children;
    let ipWhoisInformation = siteDetails[10].children;
    let fileInformation = siteDetails[18].children;

    let regex2 = /\n/gi;
    let regex3 = /"/gi;

    // Following gathers information from the desired sections from the site detail page
    let json_text = '{\n'
    json_text += '"Event_Type" : "' + document.getElementsByClassName('event-tagList')[document.getElementsByClassName('event-tagList').length-1].getElementsByTagName('span')[1].innerText + '",\n';
    json_text += '"IP" : "' + asset[1].getElementsByClassName("detail-column-9 detail-content-item-value")[0].innerText +'",\n';
    json_text += '"CName" : "' + asset[2].getElementsByClassName("detail-column-9 detail-content-item-value")[0].innerText +'",\n';
    json_text += '"ASN" : "' + asset[3].getElementsByClassName("detail-column-9 detail-content-item-value")[0].innerText +'",\n';
    json_text += '"Registrar" : "' + asset[4].getElementsByClassName("detail-column-9 detail-content-item-value")[0].innerText +'",\n';

    json_text += '"Domain_Name" : "' + whoisInformation[1].getElementsByClassName("detail-column-9 detail-content-item-value")[0].innerText +'",\n';

    json_text += '"Whois_IP" : "' + ipWhoisInformation[1].getElementsByClassName("detail-column-9 detail-content-item-value")[0].innerText +'",\n';
    json_text += '"Email" : "' + ipWhoisInformation[6].getElementsByClassName("detail-column-9 detail-content-item-value")[0].innerText +'",\n';

    json_text += '"Created_At" : "' + fileInformation[7].getElementsByClassName("detail-column-9 detail-content-item-value")[0].innerText +'",\n';

    // Will attempt to strip the message of the email if there is the enforcement email page open
    try {
        json_text += '"Message_Subject" : "' + document.getElementsByName('emailSubject')[0].value.replace(regex2, '\\n').replace(regex3, '\\"') + '",\n';
        json_text += '"Message_Body" : "' + document.getElementsByClassName('editor-text enforcement-editor-text')[0].innerText.replace(regex2, '\\n').replace(regex3, '\\"') + '"';
        if (document.getElementsByName('filter')[0].value == '')
            throw new CustomError('templateError', 'no template selected');
    } catch {
        dataPort.postMessage({log: 'ALERT:ERROR: Enforce email page not open/template not chosen. Some data could not be retrieved.'});
        json_text = json_text.substr(0, json_text.length-2);
    }

    json_text += '}'

    console.log(json_text);
    dataPort.postMessage({data: json_text, log: json_text});
};