go();

function go() {
	let formPort = chrome.runtime.connect({name: "formPort"});

	formPort.onMessage.addListener(function(msg) {
		singleFormIDsUsed(msg.json, formPort);
	});
};

//////////////////    FILLS OUT FORM WITH 1 DOCUMENT.FORM IF ITS ELEMENTS HAVE ID'S WITH MEANINGFUL NAMES   ///////////////////////////

function singleFormIDsUsed(json_obj, formPort) {
    let desiredForm = 0;
	if (document.forms.length != 1) {
		formPort.postMessage({log: 'more than one form on document.'});
		desiredForm = findForm();
		formPort.postMessage({log: 'Attempted to find form, using form: ' + desiredForm});
	}
	for (field of document.forms[desiredForm].elements) {
		if (field.id.toLowerCase().includes('name')) {
			field.value = 'John Doe';
		} else if (field.id.toLowerCase().includes('email')) {
			field.value = 'johndoe@riskiq.net';
		} else if (field.id.toLowerCase().includes('subject')) {
			field.value = json_obj.Message_Subject;
		} else if (field.id.toLowerCase().includes('message') || 
				   field.id.toLowerCase().includes('log') || 
				   field.id.toLowerCase().includes('comment') ||
                   field.id.toLowerCase().includes('detail')) {
			field.value = json_obj.Message_Body;
		} else if (field.id.toLowerCase().includes('department') ||
				   field.id.toLowerCase().includes('dep') || 
				   field.id.toLowerCase().includes('dpt') ||
				   field.id.toLowerCase().includes('type') ||
				   field.id.toLowerCase().includes('category')) {
			for (option of field.options) {
				if (option.innerText.toLowerCase().includes('abuse'))
					option.selected = true;
				else if (option.innerText.toLowerCase().includes('phish') && json_obj.Event_Type.toLowerCase().includes('phish'))
					option.selected = true;
				else if ((option.innerText.toLowerCase().includes('copyright') || option.innerText.toLowerCase().includes('legal')) && json_obj.Event_Type.toLowerCase().includes('infringement'))
					option.selected = true;
			}
		} else if (field.id.toLowerCase().includes('priority')) {
			for (option of field.options) {
				if (option.innerText.toLowerCase().includes('high'))
					option.selected = true;
			}
		} else if (field.id.toLowerCase().includes('url')) {
		    field.value = json_obj.CName;
		} else if (field.id.toLowerCase().includes('ip')) {
			field.value = json_obj.IP;
		} else if (field.id.toLowerCase().includes('date')) {
			let d = new Date();
			field.value = d.getMonth() + '/' + d.getDate() + '/' + d.getFullYear();
		} else if (field.id.toLowerCase().includes('domain')) {
			field.value = json_obj.Domain_Name;
		} else if (field.id.toLowerCase().includes('phone')) {
			field.value = '1.888.415.4447'; // found this num on riskiq website
		}
	}

	console.log(json_obj);
	formPort.postMessage({log: 'sendData.js ran'});
};


// Attempts to locate the index of the correct form based on field names
function findForm() {
    let form = 0;
    for (let i = 0; i < document.forms.length; i++){
        for (field of document.forms[i].elements){
            if (document.forms[i].length < 3){
                //Skip this form, it is probably for log in credentials
                break;
            }
            else if (field.id.toLowerCase().includes('name')) {
                form = i;
            } else if (field.id.toLowerCase().includes('email')) {
                form = i;
            } else if (field.id.toLowerCase().includes('subject')) {
                form = i;
            } else if (field.id.toLowerCase().includes('message') ||
                field.id.toLowerCase().includes('log') ||
                field.id.toLowerCase().includes('comment') ||
                field.id.toLowerCase().includes('detail')) {
                form = i;
            }
        }
    }
    return form;
};




















