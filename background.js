//////////////////    DEFINES WHICH PAGES CAN USE EXTENSION   ///////////////////////////

chrome.runtime.onInstalled.addListener(function() {
	console.log('BEGINNING');
	chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
		chrome.declarativeContent.onPageChanged.addRules([{
			conditions: [
				new chrome.declarativeContent.PageStateMatcher({
					pageUrl: {urlPrefix: 'https://app.riskiq.net/a/main'}
				}),
				new chrome.declarativeContent.PageStateMatcher({
					pageUrl: {urlPrefix: 'https://my.hostafrica.co.za/submitticket'}
				}),
				new chrome.declarativeContent.PageStateMatcher({
					pageUrl: {urlPrefix: 'https://www.billing.blueangelhost.com/submitticket'}
				}),
				new chrome.declarativeContent.PageStateMatcher({
					pageUrl: {urlPrefix: 'https://www.liquidweb.com/contact-us/abuse/'}
				}),
				new chrome.declarativeContent.PageStateMatcher({
					pageUrl: {urlPrefix: 'https://abuse.web.com/'}
				}),
                new chrome.declarativeContent.PageStateMatcher({
                    pageUrl: {urlPrefix: 'https://www.cloudflare.com/abuse/form'}
                }),
				new chrome.declarativeContent.PageStateMatcher({
					pageUrl: {urlEquals: 'https://support.namecheap.com/index.php?/Tickets/Submit&_ga=2.191901674.1504004949.1567118322-1554514284.1567118322'}
				}),
				new chrome.declarativeContent.PageStateMatcher({
					pageUrl: {urlEquals: 'https://support.namecheap.com/index.php?/Tickets/Submit/RenderForm'}
				})],
			actions: [new chrome.declarativeContent.ShowPageAction()]
		}]);
	});
});

//////////////////    LOGS ALL MESSAGES TO BACKGROUND CONSOLE - IF MESSAGE BEGINS WITH 'ALERT:' IT WILL ALSO APPEAR AS A PAGE ALERT   ///////////////////////////

chrome.runtime.onConnect.addListener(function(port) {
	port.onMessage.addListener(function(msg) {
		console.log(msg.log);
		if (msg.log.substr(0, 6) == 'ALERT:') {
			chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
		        chrome.tabs.executeScript(tabs[0].id, {code: 'alert("' + msg.log.substr(6) + '");'}); 
		    });
		}
	});
});