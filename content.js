//////////////////    INITIALIZATIONS/SETUP   ///////////////////////////

let reportButton = document.getElementById('reportButton');
let fillButton = document.getElementById('fillButton');
let imageLink = document.getElementById('imageLink');

dataType = null;
activeTab = null;
json_obj = null;

chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    activeTab = tabs[0];
});


// so that all logs go to background page, accessible in one place
let logPort = chrome.runtime.connect({name: "logPort"});
function log(str) {
    logPort.postMessage({log: str});
};


//////////////////    BUTTONS SET PROCESS IN MOTION   ///////////////////////////

reportButton.onclick = function() {
    if (activeTab.url.includes('https://app.riskiq.net/a/main/index#/events/')) {
        log('case: report');
        dataType = 'openForm';
        getData();
    }
}

fillButton.onclick = function() {
    dataType = 'fill';
    log('case: fill')
    getData();
};

imageLink.onclick = function() {
    dataType = 'image';
    log('case: image')
    getData();
};

//////////////////    GETDATA.JS IS ACTIVATED ON RISKIQ EVENT PAGE   ///////////////////////////

function getData() {
    chrome.tabs.query({}, function(tabs) {
        // cycle through tabs in window until riskiq tab is found
        for (let i = 0; i < tabs.length; i++) {
            if (tabs[i].url.includes('https://app.riskiq.net/a/main/index#/events/') && tabs[i].url.length > 45) {
                // tab found, calls receiveDataFillForm to handle data
                log('found riskiq tab');

                chrome.tabs.executeScript(tabs[i].id, {file: 'getData.js'}); // , receiveDataFillForm
                return;
            }
        }
        // riskiq page not found
        log('ALERT:ERROR: RiskIQ event page is not open. Open event page, click actions -> enforce, and choose a template. Then, try again.');
    });  
    

};

//////////////////    RECIEVES DATA SENT BACK FROM GETDATA.JS (POTENTIALLY ALSO SENDDATA IF NECESSARY LATER)   ///////////////////////////

chrome.runtime.onConnect.addListener(function(port) {
    if (port.name == 'dataPort') {
        port.postMessage({type: dataType});
        port.onMessage.addListener(function(msg) {
            receiveDataFillForm(msg.data);
        });
    } else if (port.name == 'formPort') {
        port.postMessage({json: json_obj});
        port.onMessage.addListener(function(msg) {

        });
    } else {
        log('port not recognized :/');
    }
});

//////////////////    * ACTION *   ///////////////////////////

function receiveDataFillForm(json_str) {
    json_obj = JSON.parse(json_str);
    switch (dataType) {
        case 'image':
            if (json_obj.Image_Source != '') {
                log('opening image');
                chrome.tabs.create({url: json_obj.Image_Source});
            } else {
                log('ALERT:There are no images to download for this event.')
            }
            break;
        case 'fill':
            log('filling');
            // ACTIVATES SENDDATA.JS ON FORM PAGE
            chrome.tabs.executeScript(activeTab.id, {file: 'sendData.js'});
            break;
        case 'openForm':
            log('attempt open form');
            checkEmail(json_obj.Registrar_Email);
            break;
        default:
            log('DEFAULT - SOMETHING IS WRONG');
            break;
    }
    //Adds select items to the check data list
    document.getElementById("registrarVal").innerHTML = json_obj.Registrar;
    document.getElementById("abuseValue").innerHTML = json_obj.Event_Type;
    document.getElementById("ipaddrValue").innerHTML = json_obj.IP;
    document.getElementById("urlValues").innerHTML = json_obj.CName;
};


//////////////////    REGISTRAR EMAIL SWITCH CASE   ///////////////////////////

function checkEmail(email) {
    switch(email){
        case "abuse@godaddy.com":
            window.open("https://supportcenter.godaddy.com/AbuseReport/");
            break;
        case "abuse@incapsula.com":
            window.open("https://my.imperva.com");
            break;
        case "abusencc@interserver.net":
            window.open("https://www.interserver.net/contact-information.html");
            break;
        case "onlinenic-partner@onlinenic.com":
            window.open("https://support.onlinenic.com/");
            break;
        case "abuse@liquidweb.com":
            window.open("https://www.liquidweb.com/contact-us/");
            break;
        case "abuse-iplan@IPLAN.COM.AR":
            window.open("https://www.iplan.com.ar/Iplan/Contacto");
            break;
        case "abuse@digitalocean.com":
            window.open("https://www.digitalocean.com/company/contact/#abuse");
            break;
        case "abuse@limestonenetworks.com":
            window.open("https://www.limestonenetworks.com/about/report-abuse.html");
            break;
        case "abuse@ovh.net":
            window.open("https://www.ovh.com/world/abuse/");
            break;
        case "intl-abuse@list.alibaba-inc.com":
            window.open("https://www.alibabacloud.com/report#abuse");
            break;
        case "google-cloud-compliance@google.com":
            window.open("https://support.google.com/legal/troubleshooter/1114905#ts=1115658%2C1115684");
            break;
        case "abuse@abusehost.ru":
            window.open("http://abusehost.ru/");
            break;
        case "domain-admin@yola.com":
            window.open("https://www.yola.com/support/contact/report-abuse#phishing");
            break;
        case "abuse@microsoft.com":
            window.open("https://portal.msrc.microsoft.com/en-us/engage/cars");
            break;
        case "andys@co.ru":
            window.open("https://app.convercent.com/en-us/LandingPage/21dbfbe1-7697-e511-8123-00155d623368");
            break;
        case "abuse-contact@publicdomainregistry.com":
            window.open("https://publicdomainregistry.com/report-abuse-complain/");
            break;
        case "abuse@turnkeyinternet.net":
            window.open("https://helpdesk.turnkeyinternet.net/Tickets/Submit");
            break;
        case "abuse@namesilo.com":
            window.open("https://new.namesilo.com/phishing_report.php");
            break;
        case "abuse@quadranet.com":
            window.open("https://www.quadranet.com/contact");
            break;
        case "abuse@namecheap.com":
            window.open("https://support.namecheap.com/index.php?/Tickets/Submit&_ga=2.188721002.372030028.1553642554-811107515.1553642554");
            break;
        case "abuse@amazonaws.com":
            window.open("https://aws.amazon.com/forms/report-abuse");
            break;
        case "abuse@staff.aruba.it":
            window.open("https://www.aruba.it/en/report-abuse.aspx");
            break;
        case "abuse@szervernet.hu":
            window.open("https://www.szervernet.hu/kapcsolat");
            break;
        case "alexx.person@gmail.com":
            window.open("https://internetprivatehosting.com/#");
            break;
        case "abuse@web.com":
            window.open("https://abuse.web.com/");
            break;
        case "support@hostafrica.co.za":
            window.open("https://my.hostafrica.co.za/submitticket.php?step=2&deptid=6");
            break;
        case "abuse@blueangelhost.com":
            window.open("https://www.billing.blueangelhost.com/submitticket.php?step=2&deptid=10");
            break;
        default:
            if (email == '') 
                log('ALERT:There is no registrar email available, therefore we are unable to bring up the appropriate form, if there is one.')
            else
                log('ALERT:We are unable to match the registrar email to a webform. ')
            break;
    }
}

























